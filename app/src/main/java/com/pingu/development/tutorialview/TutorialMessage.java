package com.pingu.development.tutorialview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Created by gabi on 01.05.15.
 */
public class TutorialMessage extends LinearLayout {
    private static final String DEFAULT_BUTTON_TEXT = "OK";
    public TutorialMessage(Context context, AttributeSet attributes) throws NoSuchFieldException {
        super(context,attributes);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.tutorial_message, this);
        TypedArray attributesArray = context.getTheme().obtainStyledAttributes(
                attributes,
                R.styleable.TutorialMessage,
                0,
                0);
        try{
            String message = attributesArray.getString(R.styleable.TutorialMessage_tutorial_message);
            if(message == null){
                throw new NoSuchFieldException("Message Undefined");
            }
            String button_text = attributesArray.getString(R.styleable.TutorialMessage_tutorial_button_text);
            if(button_text == null){
                button_text = DEFAULT_BUTTON_TEXT;
            }
            ViewGroup viewGroup = (ViewGroup)getChildAt(0);
            ((TextView)viewGroup.findViewById(R.id.message)).setText(message);
            ((TextView)viewGroup.findViewById(R.id.button3)).setText(button_text);
        } finally {
            attributesArray.recycle();
        }
    }
    public TutorialMessage(Context context, String message, String buttonText){
        super(context);
        if(buttonText == null){
            buttonText = DEFAULT_BUTTON_TEXT;
        }
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.tutorial_message,this);
        ((TextView)findViewById(R.id.message)).setText(message);
        ((TextView)findViewById(R.id.button3)).setText(buttonText);
    }
    public void setOnClickListener(OnClickListener listener){
        findViewById(R.id.button3).setOnClickListener(listener);
    }
}
