package com.pingu.development.tutorialview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

/**
 * Created by gabi on 01.05.15.
 */
//CustomView that highlights the rectangle of a given view
public class ShadowView extends LinearLayout {
    private int aroundId = -1;
    //private boolean isSecondRoundCompleted = false;
    //private int colorId = -1;
    public ShadowView(Context context, AttributeSet attributes) throws NoSuchFieldException {
        super(context, attributes);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.shadow_view, this);
        TypedArray attributesArray = context.getTheme().obtainStyledAttributes(
                attributes,
                R.styleable.Shadow,
                0,0);
        try{
            aroundId = attributesArray.getResourceId(R.styleable.Shadow_shadow_view_around,-1);
            if(aroundId == -1){
                throw new NoSuchFieldException("No highlighted item defined");
            }
            int colorId = attributesArray.getColor(R.styleable.Shadow_shadow_view_color,-1);
            colorId = colorId == -1 ? context.getResources().getColor(R.color.shadow_color) : colorId;
            ViewGroup viewGroup = (ViewGroup) getChildAt(0);

            viewGroup.findViewById(R.id.shadow_around_left).setBackgroundColor(colorId);
            viewGroup.findViewById(R.id.shadow_around_right).setBackgroundColor(colorId);

            viewGroup.findViewById(R.id.shadow_around_top).setBackgroundColor(colorId);
            viewGroup.findViewById(R.id.shadow_around_center).setBackgroundColor(context.getResources().getColor(R.color.transparent));
            viewGroup.findViewById(R.id.shadow_around_bottom).setBackgroundColor(colorId);


        } finally {
            attributesArray.recycle();
        }
    }
    public ShadowView(Context context, int colorId, int aroundId){
        super(context);
        this.aroundId = aroundId;
        colorId = colorId == -1 ? context.getResources().getColor(R.color.shadow_color) : colorId;
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.shadow_view, this);
        ViewGroup viewGroup = (ViewGroup) getChildAt(0);

        viewGroup.findViewById(R.id.shadow_around_left).setBackgroundColor(colorId);
        viewGroup.findViewById(R.id.shadow_around_right).setBackgroundColor(colorId);

        viewGroup.findViewById(R.id.shadow_around_top).setBackgroundColor(colorId);
        viewGroup.findViewById(R.id.shadow_around_center).setBackgroundColor(context.getResources().getColor(R.color.transparent));
        viewGroup.findViewById(R.id.shadow_around_bottom).setBackgroundColor(colorId);
    }


    //We have to wait for onLayout to know the exact position of the view to highlight
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        final int width = right -left;
        final int height = top - bottom;
        final View view = getRootView().findViewById(aroundId);
        ViewGroup viewGroup = (ViewGroup) getChildAt(0);
        int relativeLeft = getRelativeLeft(view, getRootView())- getRelativeLeft(viewGroup.getChildAt(0), getRootView());

        //We have to take into account the action bar
        int relativeTop = getRelativeTop(view, getRootView())-getRelativeTop(viewGroup.getChildAt(0),getRootView());
        View topView = viewGroup.findViewById(R.id.shadow_around_top);
        View midLayout = viewGroup.findViewById(R.id.shadow_around_mid_layout);
        View leftView = viewGroup.findViewById(R.id.shadow_around_left);
        View centerView = viewGroup.findViewById(R.id.shadow_around_center);
        ViewGroup.LayoutParams layoutParams = topView.getLayoutParams();
        if(layoutParams.height != relativeTop){
            layoutParams.height = relativeTop;
            layoutParams = midLayout.getLayoutParams();
            layoutParams.height = view.getHeight();
            layoutParams = leftView.getLayoutParams();
            layoutParams.width = relativeLeft;
            layoutParams = centerView.getLayoutParams();
            layoutParams.width = view.getWidth();
            // isSecondRoundCompleted = false;
            measureChild(viewGroup,width, height );
            super.onLayout(changed, left,top,right,bottom);
        } else {
            //isSecondRoundCompleted = true;
            super.onLayout(changed, left, top, right, bottom);
        }

    }

    /*This is Left position relative to an ancestor.*/
    public int getRelativeLeft(View descendantView, View ancestorView) {
        if (descendantView.getParent() == ancestorView)
            return descendantView.getLeft();
        else
            return descendantView.getLeft() + getRelativeLeft((View) descendantView.getParent(),ancestorView);
    }

    /*This is Top with respect to an ancestor*/
    public int getRelativeTop(View descendantView, View ancestorView) {
        if (descendantView.getParent() == ancestorView.getRootView())
            return descendantView.getTop();
        else
            return descendantView.getTop() + getRelativeTop((View) descendantView.getParent(),ancestorView);
    }
    //public boolean isSecondRoundCompleted(){return isSecondRoundCompleted;}
    public void setOnClickListener(OnClickListener listener){
        ViewGroup viewGroup = (ViewGroup)getChildAt(0);
        viewGroup.findViewById(R.id.shadow_around_left).setOnClickListener(listener);
        viewGroup.findViewById(R.id.shadow_around_right).setOnClickListener(listener);

        viewGroup.findViewById(R.id.shadow_around_top).setOnClickListener(listener);
        viewGroup.findViewById(R.id.shadow_around_center).setOnClickListener(listener);
        viewGroup.findViewById(R.id.shadow_around_bottom).setOnClickListener(listener);
    }
}
