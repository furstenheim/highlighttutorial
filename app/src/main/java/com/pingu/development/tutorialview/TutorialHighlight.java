package com.pingu.development.tutorialview;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

/**
 * Created by gabi on 01.05.15.
 */
public class TutorialHighlight extends LinearLayout {
    final int aroundId;
    private int tutorialId = 5;
    private int shadowId = 6;
    private RelativeLayout.LayoutParams layoutParams = new RelativeLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT);
    public TutorialHighlight(Context context, AttributeSet attributes) throws NoSuchFieldException{
        super(context, attributes);
        LayoutInflater inflater = LayoutInflater.from(context);
        inflater.inflate(R.layout.tutorial_highlight,this);
        TypedArray attributesArray = context.getTheme().obtainStyledAttributes(
                attributes,
                R.styleable.TutorialHighlight,
                0,0);
        try{
            aroundId = attributesArray.getResourceId(R.styleable.TutorialHighlight_highlight_around,-1);
            if(aroundId == -1){
                throw new NoSuchFieldException("No highlighted item defined");
            }
            int colorId = attributesArray.getColor(R.styleable.TutorialHighlight_highlight_color,-1);
            String message = attributesArray.getString(R.styleable.TutorialHighlight_highlight_message);
            if(message == null){
                throw new NoSuchFieldException("Message Undefined");
            }
            String button_text = attributesArray.getString(R.styleable.TutorialHighlight_highlight_button_text);
            ShadowView shadowView = new ShadowView(context, colorId, aroundId);
            shadowView.setLayoutParams(new LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
            //noinspection ResourceType
            shadowView.setId(shadowId);
            ((ViewGroup)getChildAt(0)).addView(shadowView, 0);
            View tutorialView = new TutorialMessage(context, message, button_text);
            //noinspection ResourceType
            tutorialView.setId(tutorialId);
            layoutParams = new RelativeLayout.LayoutParams(LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            tutorialView.setLayoutParams(layoutParams);
            ((ViewGroup)getChildAt(0)).addView(tutorialView);

        } finally {
            attributesArray.recycle();
        }
    }



    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        //noinspection ResourceType
        View tutorialView = findViewById(tutorialId);
        layoutParams = (RelativeLayout.LayoutParams) tutorialView.getLayoutParams();
        if(layoutParams.topMargin == 0 && layoutParams.bottomMargin == 0){
            final int height = top - bottom;
            final int width = right - left;
            final int measuredTutorialHeight = tutorialView.getMeasuredHeight();
            View view = getRootView().findViewById(aroundId);
            int relativeTop = getRelativeTop(view, getRootView())-getRelativeTop(this,getRootView());
            int relativeLeft = getRelativeLeft(view, getRootView())- getRelativeLeft(getChildAt(0), getRootView());

            //Minus sign so it is positive
            int relativeBottom = -(height + relativeTop + view.getHeight());
            //Message goes on top, there is enough space

            //We try on top
            if(relativeTop > measuredTutorialHeight && relativeTop > relativeBottom){

                layoutParams.topMargin = (relativeTop-measuredTutorialHeight)/2;
                measureChild(getChildAt(0), width, height);
                super.onLayout(changed,left,right,top,bottom);

            }
            //We try down
            else if (relativeBottom > measuredTutorialHeight) {

                layoutParams.topMargin = relativeTop + view.getHeight() + (relativeBottom - measuredTutorialHeight)/2;
                measureChild(getChildAt(0), width, height);
                super.onLayout(changed,left,right,top,bottom);

            }
            //We try left
            else if ( relativeLeft > width / 3 ) {
                layoutParams.topMargin = (-measuredTutorialHeight-height)/2;
                layoutParams.rightMargin = width - relativeLeft;
                measureChild(getChildAt(0), width, height);
                super.onLayout(changed,left,right,top,bottom);
            }
            //We try right
            else if (relativeLeft + view.getWidth() < width * 2/3){
                layoutParams.topMargin = (-measuredTutorialHeight-height)/2;
                layoutParams.leftMargin = relativeLeft + view.getWidth();
                measureChild(getChildAt(0), width, height);
                super.onLayout(changed,left,right,top,bottom);
            }
            //The view is so big, we just place it somewhere
            else {
                layoutParams.topMargin = (-measuredTutorialHeight-height)/2;
                layoutParams.rightMargin = width / 2;
                measureChild(getChildAt(0), width, height);
                super.onLayout(changed,left,right,top,bottom);
            }

        } else {
            super.onLayout(changed,left,top,right,bottom);

        }
    }

    public int getRelativeTop(View descendantView, View ancestorView) {
        if (descendantView.getParent() == ancestorView.getRootView())
            return descendantView.getTop();
        else
            return descendantView.getTop() + getRelativeTop((View) descendantView.getParent(),ancestorView);
    }

    /*This is Left position relative to an ancestor.*/
    public int getRelativeLeft(View descendantView, View ancestorView) {
        if (descendantView.getParent() == ancestorView)
            return descendantView.getLeft();
        else
            return descendantView.getLeft() + getRelativeLeft((View) descendantView.getParent(),ancestorView);
    }

    public void setOnClickListener(OnClickListener listener){
        //noinspection ResourceType
        findViewById(shadowId).setOnClickListener(listener);
        //noinspection ResourceType
        findViewById(tutorialId).setOnClickListener(listener);
    }
}

